package com.example.qrscanner.activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import com.example.qrscanner.R;
import com.example.qrscanner.database.TblQR;
import com.example.qrscanner.model.QRModel;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class GenerateActivity extends BaseActivity {
    ImageView ivQR,ivCopy,ivShare;
    EditText etQR;
    Button btnGenerate, btnWWW, btnCOM,btnClean,btnPNG,btnJPEG,btnWEBP;
    LinearLayout linearLayoutQRImage, linearLayoutSHortURL;
    TextView tvShortURL;
    String a = "ABC";
    QRModel qrModel;
    Integer b = 1;
    ArrayList<QRModel> list = new ArrayList<>();


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.histry:
                Intent intent = new Intent(GenerateActivity.this, HistoryActivity.class);
                startActivity(intent);
                return true;
//            case R.id.help:
//                showHelp();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_code);
        setUpActionBar(getString(R.string.qr_generator_text), true);
        initialisation();
        getDataForUpdate();

        btnGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidUser()){

//                    for(int i = 0; i < list.size(); i++ ){
//                        if (list.get(i).getQRName().equals(etQR.getText().toString())){
//                            Toast.makeText(getApplicationContext(), "QRRRR",Toast.LENGTH_SHORT).show();
//                        }
//
//                    }


                    if ( b.equals(1)  ||  !a.equals(etQR.getText().toString())){

                        DateFormat df = new SimpleDateFormat(getString(R.string.time_formate));
                        String date = df.format(Calendar.getInstance().getTime());

                        QRCodeButton(date);
                        linearLayoutQRImage.setVisibility(View.VISIBLE);
                        linearLayoutSHortURL.setVisibility(View.VISIBLE);
                        tvShortURL.setText(etQR.getText().toString());
                        tvShortURL.setMovementMethod(new ScrollingMovementMethod());

                    }else
                    {

                        Toast.makeText(getApplicationContext(), "All ready Generated this QR", Toast.LENGTH_LONG).show();
                    }









                }

            }
        });

        btnWWW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a = etQR.getText().toString();
                etQR.setText("www." + a);
            }
        });

        btnCOM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a = etQR.getText().toString();
                etQR.setText(a + ".com");

            }
        });

        ivCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doCopy(etQR.getText().toString());
            }
        });

        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doShare(etQR.getText().toString());
            }
        });



//        btnClean.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                etQR.setText("");
//            }
//        });



    }

    public void initialisation() {

        ivQR = findViewById(R.id.ivActQR);
        linearLayoutQRImage = findViewById(R.id.linearLayoutQRImage);
        linearLayoutSHortURL = findViewById(R.id.linearLayoutShortURL);
        etQR = findViewById(R.id.etActUrl);
        btnWWW = findViewById(R.id.btnActWww);
        btnCOM = findViewById(R.id.btnActCom);
       // btnClean = findViewById(R.id.btnClean);
        btnGenerate = findViewById(R.id.btnShortURLandQRcode);
        tvShortURL = findViewById(R.id.tvActShortURL);
        ivShare = findViewById(R.id.ivActShare);
        ivCopy = findViewById(R.id.ivActCopy);
        linearLayoutQRImage.setVisibility(View.GONE);
        linearLayoutSHortURL.setVisibility(View.GONE);
        btnPNG = findViewById(R.id.btnActPng);
        btnJPEG = findViewById(R.id.btnActJpeg);
        btnWEBP = findViewById(R.id.btnActWebp);
    }


    public void QRCodeButton(String date) {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = qrCodeWriter.encode(etQR.getText().toString(), BarcodeFormat.QR_CODE, 200, 200);
            final Bitmap bitmap = Bitmap.createBitmap(200, 200, Bitmap.Config.RGB_565);
            for (int x = 0; x < 200; x++) {
                for (int y = 0; y < 200; y++) {
                    bitmap.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            ivQR.setImageBitmap(bitmap);


            btnPNG.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File directory = new File(Environment.getExternalStorageDirectory(), "/QR/For share QR/");
                    File file = new File(directory, etQR.getText().toString()+".png");
                    try (FileOutputStream out =new FileOutputStream(file)) {
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                        Toast.makeText(getApplicationContext(), "PNG file Downloaded", Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });

            btnJPEG.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File directory = new File(Environment.getExternalStorageDirectory(), "/QR/For share QR/");
                    File file = new File(directory, etQR.getText().toString()+".jpg");
                    try (FileOutputStream out =new FileOutputStream(file)) {
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                        Toast.makeText(getApplicationContext(), "JPEG file Downloaded", Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });

            btnWEBP.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File directory = new File(Environment.getExternalStorageDirectory(), "/QR/For share QR/");
                    File file = new File(directory, etQR.getText().toString()+".webp");
                    try (FileOutputStream out =new FileOutputStream(file)) {
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                        Toast.makeText(getApplicationContext(), "WEBP file Downloaded", Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });

            long lastInsertedID = new TblQR(getApplicationContext()).insertQR(etQR.getText().toString(),0,date);

            Toast.makeText(this, lastInsertedID > 0 ? getString(R.string.QR_inserted_successfully_text) : getString(R.string.something_went_wrong_text), Toast.LENGTH_LONG).show();

            File directory = new File(Environment.getExternalStorageDirectory(), "/QR/generated QR/");
            File file = new File(directory, etQR.getText().toString()+".jpg");
            try (FileOutputStream out =new FileOutputStream(file)) {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            } catch (IOException e) {
                e.printStackTrace();
            }

//            File directory = new File(Environment.getExternalStorageDirectory(), "hrll");
//            if (!directory.exists()) {
//                directory.mkdirs();
//            }
//            File file = new File(directory, "bitmap");
//            if (!file.exists()) {
//                file.createNewFile();
//            }
//
//            try (FileOutputStream out =new FileOutputStream(file)) {
//                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
//            } catch (IOException e) {
//                e.printStackTrace();

//            }

            a = etQR.getText().toString();
            b = 0;



        }
        catch (Exception e) {
            e.printStackTrace();
        }


    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//    }
//
//    @Override
//    public void onBackPressed() {
//        Intent intent = new Intent(GenerateActivity.this,MainActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//    }

    boolean isValidUser() {
        boolean isValid = true;
        if (TextUtils.isEmpty(etQR.getText().toString())) {
            isValid = false;
            etQR.setError(getString(R.string.enter_valid_url_text));
        }

        return isValid;
    }

    void getDataForUpdate(){
        if (getIntent().hasExtra("ABC")){
            qrModel = (QRModel) getIntent().getSerializableExtra("ABC");
           etQR.setText(qrModel.getQRName());
            linearLayoutQRImage.setVisibility(View.VISIBLE);
            linearLayoutSHortURL.setVisibility(View.VISIBLE);

            File path = new File(Environment.getExternalStorageDirectory(),"/QR/generated QR");
            if(path.exists())
            {

                Bitmap mBitmap = BitmapFactory.decodeFile(path.getPath()+"/"+qrModel.getQRName()+".jpg");
                ivQR.setImageBitmap(mBitmap);

            }
            btnGenerate.setClickable(false);

        }}

}