package com.example.qrscanner.activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.qrscanner.R;
import com.example.qrscanner.adapter.QRListAdapter;
import com.example.qrscanner.database.TblQR;
import com.example.qrscanner.model.QRModel;
import java.util.ArrayList;
import butterknife.ButterKnife;

public class HistoryActivity extends BaseActivity implements QRListAdapter.InterfaceForOnClick{

    RecyclerView rcvList;
    ArrayList<QRModel> list = new ArrayList<>();
    QRListAdapter adapter;
    LinearLayout llEmpty;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clear:
//                new TblQR(this).deleteQRList();
//                llEmpty.setVisibility(View.VISIBLE);
//                list.clear();
//                adapter.notifyDataSetChanged();
////                adapter.notifyItemRemoved(list.size());
////                adapter.notifyItemRangeChanged(0,list.size());
//                Toast.makeText(this, "History deleted",Toast.LENGTH_SHORT ).show();
                showAlertDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void showAlertDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(HistoryActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are you sure want to clear history?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new TblQR(HistoryActivity.this).deleteQRList();
                        llEmpty.setVisibility(View.VISIBLE);
                        list.clear();
                        adapter.notifyDataSetChanged();
                        if (list.size() == 0) {
                            Toast.makeText(HistoryActivity.this, "History deleted", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(HistoryActivity.this, HistoryActivity.this.getString(R.string.something_went_wrong_text), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_histry);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.histry_text), true);
        rcvList = findViewById(R.id.rcvQRList);
        llEmpty = findViewById(R.id.llEmpty);
        setAdapter();
        if (list.size()==0){
            llEmpty.setVisibility(View.VISIBLE);
            rcvList.setVisibility(View.GONE);
        }
        else {
            llEmpty.setVisibility(View.GONE);
            rcvList.setVisibility(View.VISIBLE);
        }

    }

     void setAdapter() {

//         rcvList.setLayoutManager(new GridLayoutManager(this, 1));
//         list.addAll(new TblQR(this).getQRList());
//         adapter = new QRListAdapter(list, getApplicationContext(), new QRListAdapter.OnViewClickListener() {
//
//             @Override
//             public void onItemClick(int position) {
//                 Intent intent = new Intent(HistryActivity.this,ScanResultActivity.class);
//                 intent.putExtra("ABC", list.get(position));
//                 startActivity(intent);
//             }
//         } );
//
//         rcvList.setAdapter(adapter);

         list.addAll(new TblQR(this).getQRList());
         adapter = new QRListAdapter(list, this,this );
         rcvList.setAdapter(adapter);
         rcvList.setLayoutManager(new LinearLayoutManager(this));
         adapter.notifyItemRangeChanged(0, list.size());



     }

//    @Override
//    protected void onResume() {
//        super.onResume();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//    }
//
//    @Override
//    public void onBackPressed() {
//        Intent intent = new Intent(HistoryActivity.this,MainActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//    }

    @Override
    public void onclickMethodForRecylerView(int position) {

       {
           if (list.get(position).getIsScan() == 1){
               Intent intent = new Intent(HistoryActivity.this,ScanResultActivity.class);
               intent.putExtra("ABC", list.get(position));
               startActivity(intent);
           }
           else {
               Intent intent = new Intent(HistoryActivity.this,GenerateActivity.class);
               intent.putExtra("ABC", list.get(position));
               startActivity(intent);
           }
       }
    }


}