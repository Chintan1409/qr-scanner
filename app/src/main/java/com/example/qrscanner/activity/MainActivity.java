package com.example.qrscanner.activity;
import androidx.cardview.widget.CardView;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Toast;
import com.example.qrscanner.R;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class MainActivity extends BaseActivity {
    private static final int GALLERY_REQUEST_CODE = 123;

    CardView cvGenerate,cvScan,cvScanByPhoto,cvHistry;
    static Bitmap sc;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_homepage, menu);
        return true;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle item selection
//        switch (item.getItemId()) {
//            case R.id.histry:
//
//                return true;
//            case R.id.help:
//                showHelp();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpActionBar(getString(R.string.title), false);

        requestForStorageW();

        cvGenerate =findViewById(R.id.cvGenerate);
        cvScan = findViewById(R.id.cvScan);
        cvScanByPhoto =findViewById(R.id.cvScanPhoto);
        cvHistry = findViewById(R.id.cvHistry);
        cvGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,GenerateActivity.class);
                startActivity(intent);
            }
        });

        cvScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this,ScanActivity.class);
                startActivity(intent);
            }
        });
        cvScanByPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestForStorage();


            }
        });
        cvHistry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, HistoryActivity.class);
                startActivity(intent);
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {

        super.onActivityResult(reqCode, resultCode, data);
        if ( resultCode == RESULT_OK ) {
            try {
                Uri imageUri = data.getData();

                InputStream imageStream = getContentResolver().openInputStream(imageUri);
                Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                try {
                   this.sc = selectedImage;
                    String contents = null;
                    int[] intArray = new int[sc.getWidth()*sc.getHeight()];
                    sc.getPixels(intArray, 0, sc.getWidth(), 0, 0, sc.getWidth(), sc.getHeight());
                    LuminanceSource source = new RGBLuminanceSource(sc.getWidth(), sc.getHeight(), intArray);
                    BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
                    Reader reader = new MultiFormatReader();
                    Result result = reader.decode(bitmap);
                    contents = result.getText();

                    Intent intent = new Intent(MainActivity.this, ScanResultActivity.class);
                    intent.putExtra("RESULT", result.getText());

                    startActivity(intent);
                }catch (Exception e){

                    e.printStackTrace();

                }

                //  image_view.setImageBitmap(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, getString(R.string.something_went_wrong_text), Toast.LENGTH_LONG).show();

            }
        }else {
            Toast.makeText(MainActivity.this, R.string.u_have_not_picked_image_text,Toast.LENGTH_LONG).show();
        }
    }

    //    public static String decodeQRImage(String imagePath) {
//        Bitmap bMap = BitmapFactory.decodeFile(imagePath);
//        String decoded = null;
//
//        int[] intArray = new int[bMap.getWidth() * bMap.getHeight()];
//        bMap.getPixels(intArray, 0, bMap.getWidth(), 0, 0, bMap.getWidth(),
//                bMap.getHeight());
//        LuminanceSource source = new RGBLuminanceSource(bMap.getWidth(),
//    }//                bMap.getHeight(), intArray);
//        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
//
//        Reader reader = new QRCodeReader();
//        try {
//            Result result = reader.decode(bitmap);
//            decoded = result.getText();
//        } catch (NotFoundException e) {
//            e.printStackTrace();
//        } catch (ChecksumException e) {
//            e.printStackTrace();
//        } catch (FormatException e) {
//            e.printStackTrace();
//        }
//        return decoded;


    private void requestForStorage() {
        Dexter.withContext(this).withPermission(Manifest.permission.READ_EXTERNAL_STORAGE).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, getString(R.string.Pick_image_text)),GALLERY_REQUEST_CODE);


//                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//                photoPickerIntent.setType("image/*");
//                startActivityForResult(photoPickerIntent, 1000);
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                Toast.makeText(MainActivity.this, R.string.storage_permission_is_requied_text , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();
    }
    private void requestForStorageW() {
        Dexter.withContext(this).withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {

                File directory = new File(Environment.getExternalStorageDirectory(), "/QR/generated QR");
                File directory1 = new File(Environment.getExternalStorageDirectory(), "/QR/scanned QR");
                File directory2 = new File(Environment.getExternalStorageDirectory(), "/QR/For share QR");
                if (!directory.exists()) {
                    directory.mkdirs();
                }
                if (!directory1.exists()) {
                    directory1.mkdirs();
                }
                if (!directory2.exists()) {
                    directory2.mkdirs();
                }
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                Toast.makeText(MainActivity.this, R.string.storage_permission_is_requied_text , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();
    }

    public Bitmap sendImg(){
        return sc;
    }

}