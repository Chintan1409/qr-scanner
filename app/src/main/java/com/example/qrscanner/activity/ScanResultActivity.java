package com.example.qrscanner.activity;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.cardview.widget.CardView;
import com.example.qrscanner.R;
import com.example.qrscanner.database.TblQR;
import com.example.qrscanner.model.QRModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

    public class ScanResultActivity extends BaseActivity {
        CardView cvSearch, cvShare, cvCopy;
        TextView txtView,tvShortURL;
        ImageView ivCopy,ivShare,ivQR;
        QRModel qrModel;
        static String da;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_bar, menu);
        return true;
    }
        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.histry:
                    Intent intent = new Intent(ScanResultActivity.this, HistoryActivity.class);
                    startActivity(intent);
                    return true;
//            case R.id.help:
//                showHelp();
//                return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        }

//    @Override
//    protected void onResume() {
//        super.onResume();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//    }

    @SuppressLint("WrongThread")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_result);
        setUpActionBar(getString(R.string.scan_result_text), true);
        initialisation();
        getDataForUpdate();



        Bundle bundle = getIntent().getExtras();
        final String message = bundle.getString("RESULT");


        if (bundle.getString("RESULT") != null) {

//            Intent intent = getIntent();
//            Bitmap bitmap = (Bitmap) intent.getParcelableExtra("BitmapImage");

            Bitmap img = new MainActivity().sendImg();
            ivQR.setImageBitmap(img);
            File directory = new File(Environment.getExternalStorageDirectory(), "/QR/scanned QR/");
            File file = new File(directory, message+".jpg");
            try (FileOutputStream out =new FileOutputStream(file)) {
                img.compress(Bitmap.CompressFormat.PNG, 100, out);
            } catch (IOException e) {
                e.printStackTrace();
            }
            txtView.setText(message);
            txtView.setMovementMethod(new ScrollingMovementMethod());
            tvShortURL.setText(message);
            tvShortURL.setMovementMethod(new ScrollingMovementMethod());

            DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm");
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                this.da = df.format(Calendar.getInstance().getTime());
            }
            long lastInsertedID = new TblQR(getApplicationContext()).insertQR(txtView.getText().toString(),1,da);
            Toast.makeText(this, lastInsertedID > 0 ? getString(R.string.QR_inserted_successfully_text) : getString(R.string.something_went_wrong_text), Toast.LENGTH_LONG).show();
        }

        final String m1 = txtView.getText().toString();

        cvCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doCopy(m1);
            }
        });

        ivCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doCopy(m1);
            }
        });

        cvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSerch(m1);
            }
        });

        cvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doShare(m1);
            }
        });

        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doShare(m1);
            }
        });

    }

    public void initialisation() {
        cvSearch = findViewById(R.id.cvActSearch);
        cvCopy = findViewById(R.id.cvActCopy);
        cvShare = findViewById(R.id.cvActShare);
        txtView = findViewById(R.id.tvActResultCode);
        tvShortURL = findViewById(R.id.tvActShortURL);
        ivShare = findViewById(R.id.ivActShare);
        ivCopy = findViewById(R.id.ivActCopy);
        ivQR = findViewById(R.id.ivActQRResult);
    }

    public void doSerch(final String message) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(message)));
            cvSearch.setEnabled(URLUtil.isValidUrl(txtView.toString()));
        }catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(ScanResultActivity.this, getString(R.string.something_went_wrong_text), Toast.LENGTH_LONG).show();
        }

    }

//    @Override
//    public void onBackPressed() {
//        Intent intent = new Intent(ScanResultActivity.this, MainActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//
//    }

    void getDataForUpdate(){
        if (getIntent().hasExtra("ABC")){
            qrModel = (QRModel) getIntent().getSerializableExtra("ABC");
            txtView.setText(qrModel.getQRName());
            txtView.setMovementMethod(new ScrollingMovementMethod());
            tvShortURL.setText(qrModel.getQRName());
            tvShortURL.setMovementMethod(new ScrollingMovementMethod());

            File path = new File(Environment.getExternalStorageDirectory(),"/QR/scanned QR");
            if(path.exists())
            {
                Bitmap mBitmap = BitmapFactory.decodeFile(path.getPath()+"/"+qrModel.getQRName()+".jpg");
                ivQR.setImageBitmap(mBitmap);
            }

        }}




}