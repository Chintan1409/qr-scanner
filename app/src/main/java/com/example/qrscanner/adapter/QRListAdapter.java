package com.example.qrscanner.adapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qrscanner.R;
import com.example.qrscanner.model.QRModel;
import java.util.ArrayList;

public class QRListAdapter extends RecyclerView.Adapter<QRListAdapter.ViewHolder> {

    ArrayList<QRModel> list;
    Context context;
//    OnViewClickListener onViewClickListener;
    InterfaceForOnClick a;



    public QRListAdapter(ArrayList<QRModel> list,Context context,InterfaceForOnClick a){
        this.list = list;
        this.context  = context;
        this.a = a;
//        this.onViewClickListener = onViewClickListener;
    }

//    public interface OnViewClickListener {
//
//        void onItemClick(int position);
//    }



    @NonNull
    @Override
    public QRListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View groupView = inflater.inflate(R.layout.view_row_qr_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(groupView,a);
        return viewHolder;
    }




    @Override
    public void onBindViewHolder(@NonNull QRListAdapter.ViewHolder holder, final int position) {
        QRModel qrModel = list.get(position);
        holder.tvText.setText(qrModel.getQRName());


        if (qrModel.getIsScan() == 1){
            holder.tvTime.setText(qrModel.getDate()+ "     |     " + "Scanned") ;
        }
        else{
            holder.tvTime.setText(qrModel.getDate()+ "     |     " + "Generated") ;
        }





//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (onViewClickListener != null) {
//                    onViewClickListener.onItemClick(position);
//                }
//            }
//        });


    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder  extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvText,tvTime;
        InterfaceForOnClick sc;
        ImageView imageqr;
        public ViewHolder(@NonNull View itemView, InterfaceForOnClick scs) {
            super(itemView);
            this.sc = scs;
            tvText = itemView.findViewById(R.id.tvActText);
            tvTime = itemView.findViewById(R.id.tvActTime);
            imageqr = itemView.findViewById(R.id.image_qr);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            sc.onclickMethodForRecylerView(getAdapterPosition());
        }
    }

    public interface InterfaceForOnClick
    {
        void onclickMethodForRecylerView(int position);
    }


}
