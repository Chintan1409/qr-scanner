package com.example.qrscanner.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.qrscanner.model.QRModel;
import java.util.ArrayList;

public class TblQR extends MyDatabase {

    public static final String TABLE_NAME = "TblQR ";
    public static final String QR_ID = "QRID";
    public static final String IS_SCAN = "isScan";
    public static final String DATE = "Date";
    public static final String QR_IMAGE = "QRImage";

    public static final String NAME = "QRName";

    public TblQR(Context context) {
        super(context);
    }

    public ArrayList<QRModel> getQRList(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<QRModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount() ; i++){
            QRModel qrModel = new QRModel();

            qrModel.setQRID(cursor.getInt(cursor.getColumnIndex(QR_ID)));
            qrModel.setQRName(cursor.getString(cursor.getColumnIndex(NAME)));
            qrModel.setIsScan(cursor.getInt(cursor.getColumnIndex(IS_SCAN)));
            qrModel.setDate(cursor.getString(cursor.getColumnIndex(DATE)));

            list.add(qrModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;

    }

    public void deleteQRList(){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
//      db.execSQL("delete from "+ TABLE_NAME);
        db.close();

    }

//
//    public QRModel getQRByID(int id){
//        SQLiteDatabase db  = getReadableDatabase();
//        QRModel qrModel  = new QRModel();
//        String query = "SELECT * FROM " + TABLE_NAME + "WHERE " + QR_ID + " = ?";
//        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
//        cursor.moveToFirst();
//        qrModel.setQRID(cursor.getInt(cursor.getColumnIndex(QR_ID)));
//        qrModel.setQRName(cursor.getString(cursor.getColumnIndex(NAME)));
//        qrModel.setIsScan(cursor.getInt(cursor.getColumnIndex(IS_SCAN)));
//        qrModel.setDate(cursor.getString(cursor.getColumnIndex(DATE)));
//        cursor.close();
//        db.close();
//        return qrModel;
//    }
//
    public long insertQR(String name, int isScan, String date){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, name);
        cv.put(IS_SCAN, isScan);
        cv.put(DATE, date);


        long lastInsertedQR = db.insert(TABLE_NAME, null, cv);
        db.close();
        return lastInsertedQR;
    }
//
//    public int updateQRByID(int id,String name,int isScan, String date){
//        SQLiteDatabase db = getWritableDatabase();
//        ContentValues cv = new ContentValues();
//        cv.put(NAME, name);
//        cv.put(IS_SCAN, isScan);
//        cv.put(DATE, date);
//        int lastUpdatedQR = db.update(TABLE_NAME, cv,QR_ID+ "= ?",new String[]{String.valueOf(id)});
//        db.close();
//        return lastUpdatedQR;
//    }
//
//    public int deleteQRByID(int id){
//        SQLiteDatabase db = getWritableDatabase();
//        int deletedQRID = db.delete(TABLE_NAME, QR_ID + " = ? ",new String[]{String.valueOf(id)});
//        db.close();
//        return deletedQRID;
//    }


}
