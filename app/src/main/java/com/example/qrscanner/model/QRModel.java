package com.example.qrscanner.model;

import java.io.Serializable;
import java.sql.Blob;

public class QRModel implements Serializable {

    int QRID;
    String QRName;
    int isScan;
    String Date;
    Blob QRImage;

    public int getQRID() {
        return QRID;
    }

    public void setQRID(int QRID) {
        this.QRID = QRID;
    }

    public String getQRName() {
        return QRName;
    }

    public void setQRName(String QRName) {
        this.QRName = QRName;
    }

    public int getIsScan() {
        return isScan;
    }

    public void setIsScan(int isScan) {
        this.isScan = isScan;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public Blob getQRImage() {
        return QRImage;
    }

    public void setQRImage(Blob QRImage) {
        this.QRImage = QRImage;
    }

    @Override
    public String toString() {
        return "QRModel{" +
                "QRID=" + QRID +
                ", QRName='" + QRName + '\'' +
                ", isScan=" + isScan +
                ", Date='" + Date + '\'' +
                ", QRImage=" + QRImage +
                '}';
    }


}
